common
=========

Checks if Docker is installed, if not then proceeds to install it.
Intended to be used with all hosts.

Requirements
------------

- Ubuntu or Ubuntu based distribution
- 'apt' package manager

Role Variables
--------------

docker_ce_version: Which version of Docker should be installed.

docker_ce_cli_version: Which version of the Docker CLI should be installed.

docker_update_channel: Whether to install stable, edge or nightly version of Docker.

Dependencies
------------

Example Playbook
----------------

License
-------

GPL-3.0-or-later

Author Information
------------------

Tomás Gutiérrez
