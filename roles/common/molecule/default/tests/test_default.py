import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_docker_repository(host):
    source = host.file("/etc/apt/sources.list.d/download_docker_com_linux_ubuntu.list")
    assert source.exists
    assert source.contains("https://download.docker.com/linux/ubuntu")

def test_docker_is_installed(host):
    assert host.package("docker-ce").is_installed
    assert host.package("docker-ce-cli").is_installed
    assert host.package("containerd.io").is_installed

def test_docker_running_and_enabled(host):
    assert host.service("docker").is_enabled
