import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_rancherServer(host):
    source = host.docker.get_containers(name="rancher")
    assert source.exists

def test_server_running_and_enabled(host):
    assert host.docker("rancher").is_running
