rancherServer
=========

rancherServer runs the rancher server in docker, downloads the CLI for Rancher and Kubernetes, and configures Rancher Server.

Requirements
------------

- docker installed

Role Variables
--------------

rancher_release: Whether to use stable, latest or a specific version of Rancher (2.x.x)

rancher_volume_path: Where to store configuration files for rancher

rancher_http_port: HTTP port where the rancher web server is going to be exposed

rancher_https_port: HTTPS port where the rancher web server is going to be exposed

rancher_ip_server: IP of the where the Rancher server is going to be hosted

rancher_cli_version: Version of the Rancher CLI (v2.x.x)

kubernetes_cli_version: Version of the Kubernete CLI. Latest stable version can be obtained from https://storage.googleapis.com/kubernetes-release/release/stable.txt (v.1.x.x)

Dependencies
------------

Example Playbook
----------------

    - hosts: server
      become: true
      roles:
      - rancherServer

License
-------

GPL-3.0-or-later

Author Information
------------------

Tomás Gutiérrez
