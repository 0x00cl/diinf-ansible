pipeline
=========

The role pipeline creates the environment for development. Deploys Jenkins, SonarQube and Postgres workload and services. It will also create a serviceAccount named 'diinf' that allows Jenkins to deploy Kubernetes pods in Rancher.

Requirements
------------

- Rancher server running
- Rancher cluster registered and active in Rancher server

Role Variables
--------------

rancher_ip_server: IP of the where the Rancher server is going to be hosted

rancher_cli_version: Version of the Rancher CLI (v2.x.x)

kubernetes_cli_version: Version of the Kubernete CLI. Latest stable version can be obtained from https://storage.googleapis.com/kubernetes-release/release/stable.txt (v.1.x.x)

rancher_jenkins_workload: JSON that contains the configuration of Jenkins Helm Chart

rancher_postgres_workload: JSON that contains the configuration of Postgres Helm Chart

rancher_sonarqube_workload: JSON that contains the configuration of SonarQube Helm Chart

rancher_sonarqube_port: JSON that opens SonarQube port to the Cluster containers only (service)


Dependencies
------------


Example Playbook
----------------

    - hosts: nodes
      roles:
      - pipeline

License
-------

GPL-3.0-or-later

Author Information
------------------

Tomás Gutiérrez
