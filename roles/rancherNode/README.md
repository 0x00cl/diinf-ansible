RancherNode
=========

RancherNode register nodes from the [nodes] group in the 'hosts' file into a Rancher server, creating for each node a cluster.

Requirements
------------

- docker installed
- docker pip module (Installed by the 'common' role)

Role Variables
--------------

rancher_ip_server: IP or domain where the Rancher server is located

rancher_cluster_json_body: JSON Body for the API to create a cluster in Rancher with the name cluster-{{ node_hostname }}

Dependencies
------------

Example Playbook
----------------

    - hosts: nodes
      roles:
      - rancherNode

License
-------

GPL-3.0-or-later

Author Information
------------------

Tomás Gutiérrez
