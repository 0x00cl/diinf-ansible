# DIINF-Ansible

Este proyecto es un *playbook* de Ansible creado como solución para el trabajo de tesis titulado Comparación e Integración de herramientas para el pipeline de DevOps para el proceso de desarollo y operación de software en el Departamento de Ingeniería Informática.

Este *playbook* prepara máquinas instalando Docker, luego en Docker instala Rancher en el cual se crea un *cluster* y dentro del *cluster* instala Jenkins y SonarQube los cuales vienen configurados. La solución permite finalmente que proyectos de *software* que pasen por un *pipeline* de Jenkins sean desplegados en el mismo *cluster*.

## Requerimientos

Para poder ejecutar este proyecto se necesita:
- Ubuntu 16.04 (Máquinas que serán configuradas)
- Ansible 2.8.x (Máquina que ejecuta el *playbook*)
- Conexión SSH a las máquinas a configurar
- Python 2.7 (Ubuntu 16.04 por defecto trae instalado Python)
- ufw

## Configuración

El proyecto se puede configurar a traves de las variables que estan en el directorio llamado *inventory*

### hosts

Este archivo contiene las maquinas que ejecutarán las tareas del *playbook*. Se debe designar una maquina bajo el *tag* de `[server]` y una o más maquinas bajo el *tag* de `[nodes]`

Ejemplo:

    [nodes]
    tgutierrezl@192.168.0.34
    tgutierrezl@kamino.diinf.usach.cl

    [server]
    tgutierrezl@yavin.diinf.usach.cl

Como se vé en el ejemplo se debe también colocar el usuario en la máquina a configurar y su *IP* o *URL*.

### Variables

Las varibles del proyecto se encuentran en el archivo `all.yml` dentro del directorio *group_vars*.

Este archivo contiene 6 variables de las cuales 5 son configurables.

- rancher_ip_server: Esta variable le indica a el servidor de Rancher la URL o IP en la que es desplegado.
- rancher_cli_version: Indica la version de la CLI de Rancher.
- lubernetes_cli_version: Indica la version de la CLI de Kubernetes.
- rancher_cluster_name: El nombre del cluster que se creará al momento de ejecutar el *playbook*.
- rancher_docker_release: Repositorio que utiliza para descargar docker las opciones son: `nightly`, `test` o `stable`.

## Ejecución

Para poder ejecutar este proyecto se deben haber definido todas las variables y al menos una maquina definida bajo `server` y una bajo `nodes` en el archivo *hosts*.

Para comenzar la ejecución se utiliza el comando:
`ansible-galaxy -K playbook.yaml`

La opción *-K* en el comando preguntará por la contraseña de la cuenta en la maquina remota para poder ejecutar las tareas como *sudo*.

### Nota

Rancher requiere abrir ciertos puertos para poder comunicarse entre las maquinas. https://rancher.com/docs/rancher/v2.x/en/installation/references/
